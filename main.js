let audio = document.getElementById('audio')
let playOrPause = false
const songList = [
  {
    id: 1,
    title: 'Nillili_Mambo',
    src: 'audio/Nillili_Mambo.mp3',
  },
  {
    id: 2,
    title: 'Me_Gustas_T',
    src: 'audio/Me_Gustas_T.mp3',
  },
  {
    id: 3,
    title: 'Alt-J-Breezeblock',
    src: 'audio/Alt-J-Breezeblock.mp3',
  },
  {
    id: 4,
    title: 'Aniello_Saxo',
    src: 'audio/Aniello_Saxo.mp3',
  },
  {
    id: 5,
    title: 'Beauty_Brain',
    src: 'audio/Beauty_Brain.mp3',
  },
  {
    id: 6,
    title: 'Bingo_Players',
    src: 'audio/Bingo_Players.mp3',
  },
  {
    id: 7,
    title: 'DJ_Sequence_F',
    src: 'audio/DJ_Sequence_Freaking.mp3',
  },
  {
    id: 8,
    title: 'Maduk_Nymfo',
    src: 'audio/Maduk_Nymfo.mp3',
  },
  {
    id: 9,
    title: 'Matisse_Sadko',
    src: 'audio/Matisse_Sadko.mp3',
  },
  {
    id: 10,
    title: 'Me_My_Toothbrush',
    src: 'audio/Me_My_Toothbrush.mp3',
  },
  {
    id: 11,
    title: 'Vicetone_Feat',
    src: 'audio/Vicetone_Feat.mp3',
  },
  {
    id: 12,
    title: 'Alexey_Romeo',
    src: 'audio/Alexey_Romeo.mp3',
  },
  {
    id: 13,
    title: 'Avicii_Silhouettes',
    src: 'audio/Avicii_Silhouettes.mp3',
  },
]

songList.forEach((element) => {
  document.getElementById('ul').innerHTML +=
    '<li onclick="playSongList(' + element.id + ')">' + element.title + '</ li>'
})

function playSongList(e) {
  const index = e - 1
  document.getElementById('name').textContent = songList[index].title
  audio.src = songList[index].src
  audio.play()
  document.getElementById('play-pause-button').innerHTML =
    '<i class="fa-solid fa-pause"></i>'
  return (playOrPause = true)
}

function playSong() {
  if (audio.src == '') {
    document.getElementById('name').textContent = songList[0].title
    audio.src = songList[0].src
  }
  if (!playOrPause) {
    audio.play()
    document.getElementById('play-pause-button').innerHTML =
      '<i class="fa-solid fa-pause"></i>'
    return (playOrPause = true)
  }
  if (playOrPause) {
    audio.pause()
    document.getElementById('play-pause-button').innerHTML =
      '<i class="fa-solid fa-play"></i>'
    return (playOrPause = false)
  }
}

function nextSong() {
  document.getElementById('play-pause-button').innerHTML =
    '<i class="fa-solid fa-pause"></i>'
  let music = songList.filter(function (song) {
    return song.src == 'audio' + audio.src.split('audio')[1]
  })
  if (!music.length || music[0].id == songList.length) {
    document.getElementById('name').textContent = songList[0].title
    audio.src = songList[0].src
  } else {
    document.getElementById('name').textContent = songList[music[0].id].title
    audio.src = songList[music[0].id].src
  }
  audio.play()
  return (playOrPause = true)
}

function previousSong() {
  document.getElementById('play-pause-button').innerHTML =
    '<i class="fa-solid fa-pause"></i>'
  let music = songList.filter(function (song) {
    return song.src == 'audio' + audio.src.split('audio')[1]
  })
  if (!music.length || music[0].id == 1) {
    document.getElementById('name').textContent =
      songList[songList.length - 1].title
    audio.src = songList[songList.length - 1].src
  } else {
    document.getElementById('name').textContent =
      songList[music[0].id - 2].title
    audio.src = songList[music[0].id - 2].src
  }
  audio.play()
  return (playOrPause = true)
}
